### Audio ###

# SILENCE!
# PRODUCT_PROPERTY_OVERRIDES += \
#   persist.debug.sf.noaudio=1

PRODUCT_PROPERTY_OVERRIDES += \
    af.resampler.quality=4 \
    audio.offload.buffer.size.kb=32 \
    audio.offload.gapless.enabled=true \
    use.voice.path.for.pcm.voip=true

# Check for QCOM's HW AAC encoder only when qcom.aac.encoder = true
PRODUCT_PROPERTY_OVERRIDES += \
    qcom.hw.aac.encoder=true

# 'Endfire' throws errors, none seems cool.
PRODUCT_PROPERTY_OVERRIDES += \
    ro.qc.sdk.audio.fluencetype=none \
    ro.qc.sdk.audio.ssr=false \
    tunnel.audio.encode=true \
    persist.audio.fluence.voicecall=true \
    persist.audio.fluence.voicerec=false \
    persist.audio.fluence.speaker=true \
    av.offload.enable=false

# liblge_soundnormalizerV2.so
PRODUCT_PROPERTY_OVERRIDES += \
    lge.normalizer.param=Version2/true/8.5/true/10000/1.0/3300/0.6

# Do we need this? I'm leaning towards: no.
# dolby.audio.hdmi.channels=hdmi sink channel count
# PRODUCT_PROPERTY_OVERRIDES += \
#     dolby.audio.hdmi.channels=0 \
#     dolby.audio.sink.info=speaker

# ACDB loader
PRODUCT_PROPERTY_OVERRIDES += \
    persist.audio.calfile0=/etc/acdbdata/MADAI/Bluetooth_cal.acdb \
    persist.audio.calfile1=/etc/acdbdata/MADAI/General_cal.acdb \
    persist.audio.calfile2=/etc/acdbdata/MADAI/Global_cal.acdb \
    persist.audio.calfile3=/etc/acdbdata/MADAI/Handset_cal.acdb \
    persist.audio.calfile4=/etc/acdbdata/MADAI/Hdmi_cal.acdb \
    persist.audio.calfile5=/etc/acdbdata/MADAI/Headset_cal.acdb \
    persist.audio.calfile6=/etc/acdbdata/MADAI/Speaker_cal.acdb

### Media ###
PRODUCT_PROPERTY_OVERRIDES += \
    mmp.enable.3g2=true \
    mm.enable.smoothstreaming=true \
    mm.enable.vsync.render=1 \
    mm.enable.qcom_parser=37491 \
    drm.service.enabled=true \
    media.stagefright.enable-player=true \
    media.stagefright.enable-http=true \
    media.stagefright.enable-aac=true \
    media.stagefright.enable-qcp=true \
    media.stagefright.enable-fma2dp=true \
    media.stagefright.enable-scan=true

### Display ###
PRODUCT_PROPERTY_OVERRIDES += \
    debug.sf.fb_always_on=1 \
    debug.sf.hw=1 \
    debug.egl.hw=1 \
    debug.composition.type=c2d \
    persist.hwc.mdpcomp.enable=true \
    debug.egl.trace=systrace \
    debug.mdpcomp.idletime=-1 \
    debug.mdpcomp.logs=0 \
    dev.pm.dyn_samplingrate=1 \
    ro.opengles.version=196608 \
    ro.sf.lcd_density=320 \
    vidc.debug.level=0 \
    ro.qualcomm.cabl=0

### NFC ###
PRODUCT_PROPERTY_OVERRIDES += \
    ro.nfc.port=I2C

### Dalvik ###
PRODUCT_PROPERTY_OVERRIDES += \
    dalvik.vm.checkjni=true \
    dalvik.vm.heapgrowthlimit=192m \
    dalvik.vm.heapmaxfree=16m \
    dalvik.vm.heapminfree=512k \
    dalvik.vm.heapsize=256m \
    dalvik.vm.heapstartsize=8m \
    dalvik.vm.heaptargetutilization=0.75 \
    dalvik.vm.stack-trace-file=/data/anr/traces.txt \
    persist.sys.dalvik.multithread=false

### WIFI Display ##############################################
PRODUCT_PROPERTY_OVERRIDES += \
    persist.debug.wfd.enable=1 \
    persist.sys.wfd.virtual=0

PRODUCT_PROPERTY_OVERRIDES += \
    persist.demo.hdmirotationlock=false

### Bluetooooth ###############################################
PRODUCT_PROPERTY_OVERRIDES += \
    net.bt.name=Madai \
    bluetooth.hfp.client=1 \
    ro.qualcomm.bt.hci_transport=smd

# LOGGER_*.sh
PRODUCT_PROPERTY_OVERRIDES += \
    persist.service.crash.enable=0 \
    persist.service.events.enable=6 \
    persist.service.kernel.enable=6 \
    persist.service.main.enable=6 \
    persist.service.radio.enable=6 \
    persist.service.system.enable=6 \
    persist.sys.ssr.restart_level=3

# Time Daemon
PRODUCT_PROPERTY_OVERRIDES += \
    persist.timed.enable=true

# system props for the data modules
PRODUCT_PROPERTY_OVERRIDES += \
    ro.use_data_netmgrd=true \
    persist.data.netmgrd.qos.enable=false \
    DEVICE_PROVISIONED=1

### Telephony ##########################################
#   telephony.lteOnCdmaDevice=1
#   persist.radio.add_power_save=1
#   persist.radio.adb_log_on=1
#   ro.cdma.subscribe_on_ruim_ready=true
    # persist.radio.snapshot_enabled=1 \
    # persist.radio.snapshot_timer=22 \
    # persist.data.sbp.update=0 \
    # persist.radio.rat_on=legacy
#    ro.telephony.default_cdma_sub=0 \
#   ro.telephony.get_imsi_from_sim=true \
#   persist.data.sbp.update=0 \
#   persist.radio.no_wait_for_card=1
#     ro.telephony.ril_class=LgeLteRIL

PRODUCT_PROPERTY_OVERRIDES += \
    persist.radio.apm_sim_not_pwdn=1 \
    persist.qcril.disable_retry=true \
    ro.telephony.call_ring.delay=0 \
    ro.telephony.call_ring.multiple=false \
    keyguard.no_require_sim=true \
    ril.subscription.types=NV,RUIM \
    persist.env.fastdorm.enabled=false \
    persist.radio.msgtunnel.start=false \
    persist.radio.atfwd.start=false

#  0     WCDMA preferred
#  1     GSM only
#  2     WCDMA only
#  3     GSM auto (PRL)
#  4     CDMA auto (PRL)
#  5     CDMA only
#  6     EvDo only
#  7     GSM/CDMA (PRL)
#  8     LTE/CDMA auto (PRL)
#  9     LTE/GSM auto (PRL)
# 10     LTE/GSM/CDMA auto (PRL)
# 11     LTE only
# 12     Unknown
PRODUCT_PROPERTY_OVERRIDES += \
    ro.telephony.default_network=10

# RIL
PRODUCT_PROPERTY_OVERRIDES += \
    rild.libpath=/vendor/lib/libril-qc-qmi-1.so

### ADB
PRODUCT_PROPERTY_OVERRIDES += \
    ro.adb.secure=0 \
    persist.sys.root_access=1 \
    ro.debuggable=1

# Stock Madai @ libatd_common.so -- why keep this? For ATD and pals only?
PRODUCT_PROPERTY_OVERRIDES += \
    ro.lge.factoryversion=LGL25AT-00-V100-KDI-JP-MAR-20-2015+0 \
    ro.lge.swversion=LGL25100 \
    ro.lge.swversion_rev=0 \
    ro.lge.swversion_short=V100

# GPS
PRODUCT_PROPERTY_OVERRIDES += \
    ro.gps.agps_provider=1 \
    ro.qc.sdk.izat.premium_enabled=0 \
    ro.qc.sdk.izat.service_mask=0x8 \
    persist.gps.qc_nlp_in_use=1

### SENSORS ###
# Sensor debugging
# Valid settings (and presumably what they mean):
#   0      - off
#   1      - all the things
#   V or v - verbose
#   D or d - debug
#   E or e - errors
#   W or w - warnings
#   I or i - info
#

PRODUCT_PROPERTY_OVERRIDES += \
    debug.qualcomm.sns.daemon=e \
    debug.qualcomm.sns.hal=w \
    debug.qualcomm.sns.libsensor1=e

PRODUCT_PROPERTY_OVERRIDES += \
    ro.vendor.extension_library=/vendor/lib/libqc-opt.so

### WIFI ###
PRODUCT_PROPERTY_OVERRIDES += \
    wifi.interface=wlan0 \
    wifi.supplicant_scan_interval=15 \
    net.tethering.noprovisioning=true

# QCNE (Qualcomm ConNectivity Engine)
PRODUCT_PROPERTY_OVERRIDES += \
    persist.cne.feature=0

PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.strictmode.disable=true \
    persist.sys.whitelist=/etc/whitelist_appops.xml \
    persist.sys.usb.config=mass_storage,adb \
    ro.adb.secure=0 \
    ro.secure=0

