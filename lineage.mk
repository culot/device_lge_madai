# Boot animation
TARGET_SCREEN_HEIGHT := 1280
TARGET_SCREEN_WIDTH := 720

# Screen density
PRODUCT_AAPT_CONFIG      := normal hdpi xhdpi
PRODUCT_AAPT_PREF_CONFIG := xhdpi

$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)

# Inherit from this device
$(call inherit-product, $(LOCAL_PATH)/device.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Device identifier. This must come after all inclusions
PRODUCT_NAME := lineage_madai
PRODUCT_DEVICE := madai
PRODUCT_BRAND := lge
PRODUCT_MODEL := LGL25
PRODUCT_MANUFACTURER := LGE
PRODUCT_RELEASE_NAME := LG Fx0

PRODUCT_RESTRICT_VENDOR_FILES := false

TARGET_VENDOR_PRODUCT_NAME	:= "LGL25_jp_kdi"
TARGET_VENDOR_DEVICE_NAME	:= "madai"
# TARGET_VENDOR_RELEASE_BUILD_ID := ""

PRODUCT_BUILD_PROP_OVERRIDES += \
	PRODUCT_NAME=LGL25_jp_kdi \
	BUILD_FINGERPRINT="KDDI/madai/madai:4.4.2/KVT49L/LGL25100.1426828602:user/release-keys" \
	PRIVATE_BUILD_DESC="madai-user 4.4.2 KVT49L LGL25100.1426828602 release-keys"

