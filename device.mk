#
# Copyright (C) 2013 The CyanogenMod Project
# Copyright (C) 2017 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Inherit proprietary blobs
$(call inherit-product-if-exists, vendor/lge/madai/madai-vendor.mk)

# Overlays
DEVICE_PACKAGE_OVERLAYS += $(LOCAL_PATH)/overlay

# Permissions
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.audio.low_latency.xml:system/etc/permissions/android.hardware.audio.low_latency.xml \
    frameworks/native/data/etc/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml \
    frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml \
    frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
    frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
    frameworks/native/data/etc/android.hardware.camera.full.xml:system/etc/permissions/android.hardware.camera.full.xml \
    frameworks/native/data/etc/android.hardware.camera.raw.xml:system/etc/permissions/android.hardware.camera.raw.xml \
    frameworks/native/data/etc/android.hardware.ethernet.xml:system/etc/permissions/android.hardware.ethernet.xml \
    frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
    frameworks/native/data/etc/android.hardware.nfc.hce.xml:system/etc/permissions/android.hardware.nfc.hce.xml \
    frameworks/native/data/etc/android.hardware.nfc.xml:system/etc/permissions/android.hardware.nfc.xml \
    frameworks/native/data/etc/android.hardware.opengles.aep.xml:system/etc/permissions/android.hardware.opengles.aep.xml \
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.barometer.xml:system/etc/permissions/android.hardware.sensor.barometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:system/etc/permissions/android.hardware.sensor.compass.xml \
    frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml \
    frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
    frameworks/native/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepcounter.xml:system/etc/permissions/android.hardware.sensor.stepcounter.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepdetector.xml:system/etc/permissions/android.hardware.sensor.stepdetector.xml \
    frameworks/native/data/etc/android.hardware.telephony.cdma.xml:system/etc/permissions/android.hardware.telephony.cdma.xml \
    frameworks/native/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
    frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
    frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
    frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
    frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    frameworks/native/data/etc/android.software.midi.xml:system/etc/permissions/android.software.midi.xml \
    frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
    frameworks/native/data/etc/com.android.nfc_extras.xml:system/etc/permissions/com.android.nfc_extras.xml \
    frameworks/native/data/etc/com.nxp.mifare.xml:system/etc/permissions/com.nxp.mifare.xml \
    frameworks/native/data/etc/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
    frameworks/native/data/etc/android.hardware.opengles.aep.xml:system/etc/permissions/android.hardware.opengles.aep.xml \
    frameworks/native/data/etc/android.hardware.vulkan.level-0.xml:system/etc/permissions/android.hardware.vulkan.level.xml \
    frameworks/native/data/etc/android.hardware.vulkan.version-1_0_3.xml:system/etc/permissions/android.hardware.vulkan.version.xml

# Recovery
PRODUCT_COPY_FILES += \
    device/lge/madai/rootdir/fstab.madai:recovery/root/fstab.madai \
    device/lge/madai/rootdir/fstab.twrp:recovery/root/etc/twrp.fstab \
    device/lge/madai/rootdir/init.lge.log.rc:recovery/root/init.lge.log.rc \
    device/lge/madai/rootdir/init.lge.powerlog.rc:recovery/root/init.lge.powerlog.rc \
    device/lge/madai/rootdir/init.madai.usb.rc:recovery/root/init.madai.usb.rc \
    device/lge/madai/charger/images/chargerlogo.ini:recovery/root/res/images/chargerlogo.ini \
    device/lge/madai/rootdir/ueventd.madai.rc:recovery/root/ueventd.madai.rc \
    device/lge/madai/rootdir/postrecoveryboot.sh:recovery/root/sbin/postrecoveryboot.sh \
    bionic/libc/zoneinfo/tzdata:recovery/root/system/usr/share/zoneinfo/tzdata

# Init
PRODUCT_COPY_FILES += \
    device/lge/madai/rootdir/fstab.madai:root/fstab.madai \
    device/lge/madai/rootdir/init.lge.log.rc:root/init.lge.log.rc \
    device/lge/madai/rootdir/init.lge.powerlog.rc:root/init.lge.powerlog.rc \
    device/lge/madai/rootdir/init.madai.rc:root/init.madai.rc \
    device/lge/madai/charger/images/chargerlogo.ini:root/res/images/chargerlogo.ini \
    device/lge/madai/rootdir/ueventd.madai.rc:root/ueventd.madai.rc \
    device/lge/madai/rootdir/postrecoveryboot.sh:root/sbin/postrecoveryboot.sh

PRODUCT_COPY_FILES += \
    device/lge/madai/rootdir/init.madai.bt.sh:root/init.madai.bt.sh \
    device/lge/madai/rootdir/init.madai.crda.sh:root/init.madai.crda.sh \
    device/lge/madai/rootdir/init.madai.fm.sh:root/init.madai.fm.sh \
    device/lge/madai/rootdir/init.madai.init.sh:root/init.madai.init.sh \
    device/lge/madai/rootdir/init.madai.post_boot.sh:root/init.madai.post_boot.sh \
    device/lge/madai/rootdir/init.madai.sh:root/init.madai.sh \
    device/lge/madai/rootdir/init.madai.ssr.sh:root/init.madai.ssr.sh \
    device/lge/madai/rootdir/init.madai.uicc.sh:root/init.madai.uicc.sh \
    device/lge/madai/rootdir/init.madai.usb.rc:root/init.madai.usb.rc \
    device/lge/madai/rootdir/init.madai.usb.sh:root/init.madai.usb.sh \
    device/lge/madai/rootdir/init.madai.wifi.sh:root/init.madai.wifi.sh

# ETC
PRODUCT_COPY_FILES += \
    device/lge/madai/rootdir/etc/cacert_location.pem:system/etc/cacert_location.pem \
    device/lge/madai/rootdir/etc/capability.xml:system/etc/capability.xml \
    device/lge/madai/rootdir/etc/ftm_test_config:system/etc/ftm_test_config \
    device/lge/madai/rootdir/etc/hcidump_madai.sh:system/etc/hcidump_madai.sh \
    device/lge/madai/rootdir/etc/hsic_madai.control.bt.sh:system/etc/hsic_madai.control.bt.sh \
    device/lge/madai/rootdir/etc/last_kmsg_backup.sh:system/etc/last_kmsg_backup.sh \
    device/lge/madai/rootdir/etc/lowi.conf:system/etc/lowi.conf \
    device/lge/madai/rootdir/etc/mmi.cfg:system/etc/mmi.cfg \
    device/lge/madai/rootdir/etc/msap.conf:system/etc/msap.conf \
    device/lge/madai/rootdir/etc/sensor_def_qcomdev.conf:system/etc/sensor_def_qcomdev.conf \
    device/lge/madai/rootdir/etc/thermal-engine-8226.conf:system/etc/thermal-engine-8226.conf \
    device/lge/madai/rootdir/etc/wfdconfig.xml:system/etc/wfdconfig.xml \
    device/lge/madai/rootdir/etc/wfdconfigsink.xml:system/etc/wfdconfigsink.xml \
    device/lge/madai/rootdir/etc/whitelist_appops.xml:system/etc/whitelist_appops.xml \
    device/lge/madai/rootdir/etc/xtra_root_cert.pem:system/etc/xtra_root_cert.pem \
    device/lge/madai/rootdir/etc/xtwifi.conf:system/etc/xtwifi.conf \
    device/lge/madai/rootdir/etc/param/route.xml:system/etc/param/route.xml \
    device/lge/madai/rootdir/etc/bashrc:system/etc/bashrc \
    device/lge/madai/rootdir/etc/inputrc:system/etc/inputrc \

# input
PRODUCT_COPY_FILES += \
    device/lge/madai/rootdir/usr/idc/touch_dev.idc:system/usr/idc/touch_dev.idc \
    device/lge/madai/rootdir/usr/keychars/Generic.kcm:system/usr/keychars/Generic.kcm \
    device/lge/madai/rootdir/usr/keychars/Virtual.kcm:system/usr/keychars/Virtual.kcm \
    device/lge/madai/rootdir/usr/keychars/qpnp_pon.kcm:system/usr/keychars/qpnp_pon.kcm \
    device/lge/madai/rootdir/usr/keychars/qwerty.kcm:system/usr/keychars/qwerty.kcm \
    device/lge/madai/rootdir/usr/keychars/qwerty2.kcm:system/usr/keychars/qwerty2.kcm \
    device/lge/madai/rootdir/usr/keylayout/AVRCP.kl:system/usr/keylayout/AVRCP.kl \
    device/lge/madai/rootdir/usr/keylayout/Generic.kl:system/usr/keylayout/Generic.kl \
    device/lge/madai/rootdir/usr/keylayout/gpio-keys.kl:system/usr/keylayout/gpio-keys.kl \
    device/lge/madai/rootdir/usr/keylayout/qpnp_pon.kl:system/usr/keylayout/qpnp_pon.kl \
    device/lge/madai/rootdir/usr/keylayout/qwerty.kl:system/usr/keylayout/qwerty.kl \
    device/lge/madai/rootdir/usr/keylayout/synaptics_rmi4_i2c.kl:system/usr/keylayout/synaptics_rmi4_i2c.kl \
    device/lge/madai/rootdir/usr/idc/Atmel_maXTouch_Touchscreen_controller.idc:system/usr/idc/Atmel_maXTouch_Touchscreen_controller.idc \
    device/lge/madai/rootdir/usr/idc/atmel_mxt_ts.idc:system/usr/idc/atmel_mxt_ts.idc \
    device/lge/madai/rootdir/usr/idc/atmel-touchscreen.idc:system/usr/idc/atmel-touchscreen.idc \
    device/lge/madai/rootdir/usr/idc/ft5x06_ts.idc:system/usr/idc/ft5x06_ts.idc \
    device/lge/madai/rootdir/usr/idc/ft5x0x_ts.idc:system/usr/idc/ft5x0x_ts.idc \
    device/lge/madai/rootdir/usr/idc/msg2133.idc:system/usr/idc/msg2133.idc \
    device/lge/madai/rootdir/usr/idc/qwerty2.idc:system/usr/idc/qwerty2.idc \
    device/lge/madai/rootdir/usr/idc/qwerty.idc:system/usr/idc/qwerty.idc \
    device/lge/madai/rootdir/usr/idc/sensor00fn11.idc:system/usr/idc/sensor00fn11.idc \

PRODUCT_PACKAGES += \
    com.android.future.usb.accessory \
    qti_permissions.xml

# Charger
PRODUCT_PACKAGES += charger charger_res_images

# Audio
PRODUCT_PACKAGES += \
    audiod \
    audio.a2dp.default \
    audio_policy.msm8226 \
    audio.primary.msm8226 \
    audio.r_submix.default \
    audio.usb.default

PRODUCT_PACKAGES += \
    libaudio-resampler \
    libqcompostprocbundle \
    libqcomvisualizer \
    libqcomvoiceprocessing \
    tinymix

PRODUCT_COPY_FILES += \
    device/lge/madai/rootdir/etc/audio_effects.conf:system/etc/audio_effects.conf \
    device/lge/madai/rootdir/etc/audio_policy.conf:system/etc/audio_policy.conf \
    device/lge/madai/rootdir/etc/media_codecs.xml:system/etc/media_codecs.xml \
    device/lge/madai/rootdir/etc/media_profiles.xml:system/etc/media_profiles.xml \
    device/lge/madai/rootdir/etc/mixer_paths.xml:system/etc/mixer_paths.xml

# Keystore
PRODUCT_PACKAGES += keystore.msm8226

# Keyhandler
PRODUCT_PACKAGES += com.cyanogenmod.keyhandler

# Bluetooth
PRODUCT_PACKAGES += \
    bluetooth.default \
    hwaddrs \
    libbt-vendor

# Bluetooth
PRODUCT_COPY_FILES += \
    device/lge/madai/rootdir/etc/bluetooth/audio.conf:system/etc/bluetooth/audio.conf \
    device/lge/madai/rootdir/etc/bluetooth/auto_pair_devlist.conf:system/etc/bluetooth/auto_pair_devlist.conf \
    device/lge/madai/rootdir/etc/bluetooth/auto_pairing.conf:system/etc/bluetooth/auto_pairing.conf \
    device/lge/madai/rootdir/etc/bluetooth/blacklist.conf:system/etc/bluetooth/blacklist.conf \
    device/lge/madai/rootdir/etc/bluetooth/bt_did.conf:system/etc/bluetooth/bt_did.conf \
    device/lge/madai/rootdir/etc/bluetooth/bt_stack.conf:system/etc/bluetooth/bt_stack.conf \
    device/lge/madai/rootdir/etc/bluetooth/input.conf:system/etc/bluetooth/input.conf \
    device/lge/madai/rootdir/etc/bluetooth/main.conf:system/etc/bluetooth/main.conf \
    device/lge/madai/rootdir/etc/bluetooth/network.conf:system/etc/bluetooth/network.conf

# Camera
PRODUCT_PACKAGES += \
    camera.msm8226 \
    libmm-qcamera \
    mm-qcamera-app \
    mm-vdec-omx-test \
    mm-video-driver-test \
    mm-venc-omx-test720p \
    mm-video-encdrv-test

# Ebtables
PRODUCT_PACKAGES += \
    ebtables \
    ethertypes \
    libebtc

# GPS
PRODUCT_PACKAGES += \
    gps.msm8226

PRODUCT_COPY_FILES += \
    device/lge/madai/rootdir/etc/flp.conf:system/etc/flp.conf \
    device/lge/madai/rootdir/etc/gps.conf:system/etc/gps.conf \
    device/lge/madai/rootdir/etc/izat.conf:system/etc/izat.conf \
    device/lge/madai/rootdir/etc/quipc.conf:system/etc/quipc.conf \
    device/lge/madai/rootdir/etc/sap.conf:system/etc/sap.conf

# HAL
PRODUCT_PACKAGES += \
    copybit.msm8226 \
    gralloc.msm8226 \
    hwcomposer.msm8226 \
    memtrack.msm8226

# Lights
PRODUCT_PACKAGES += \
    lights.msm8226

# Power
PRODUCT_PACKAGES += \
    power.msm8226

# QCOM Display
PRODUCT_PACKAGES += \
    libgenlock \
    libmemalloc \
    liboverlay \
    libqdutils \
    libqdMetaData

# IRSC
PRODUCT_COPY_FILES += \
    device/lge/madai/rootdir/etc/sec_config:system/etc/sec_config

# Dumpstate
PRODUCT_PACKAGES += \
    libdumpstate.madai

# Keystore
PRODUCT_PACKAGES += \
    keystore.msm8226 \
    keystore_cli \
    keymaster_test

# Lights
PRODUCT_PACKAGES += \
    lights.msm8226

# Media
PRODUCT_COPY_FILES += \
    device/lge/madai/rootdir/etc/media_codecs.xml:system/etc/media_codecs.xml \
    device/lge/madai/rootdir/etc/media_profiles.xml:system/etc/media_profiles.xml \
    system/bluetooth/data/main.le.conf:system/etc/bluetooth/main.conf

PRODUCT_COPY_FILES += \
    system/core/rootdir/ueventd.rc:root/ueventd.rc

# Omx
PRODUCT_PACKAGES += \
    libOmxCore \
    libOmxVdec \
    libOmxVenc \
    libstagefrighthw

# NFC packages
PRODUCT_PACKAGES += \
    NfcNci \
    Tag \
    nfc_nci.msm8226 \
	libnfc-nxp-nci \
    com.android.nfc_extras

TARGET_USES_QCA_NFC := false
TARGET_USES_OS_NFC := true

NFCEE_ACCESS_PATH := device/lge/madai/rootdir/etc/nfcee_access.xml

PRODUCT_COPY_FILES += \
    device/lge/madai/rootdir/etc/libnfc-brcm.conf:system/etc/libnfc-brcm.conf \
    device/lge/madai/rootdir/etc/libnfc-nxp.conf:system/etc/libnfc-nxp.conf \
    device/lge/madai/rootdir/etc/nfc-nci.conf:system/etc/nfc-nci.conf \
    device/lge/madai/rootdir/etc/nfcee_access.xml:system/etc/nfcee_access.xml

# Qcom SoftAP
PRODUCT_PACKAGES += \
    libQWiFiSoftApCfg

# Wifi
PRODUCT_PACKAGES += \
    hostapd \
    hostapd_cli \
    libwpa_client \
    wpa_cli \
    wpa_supplicant

PRODUCT_PACKAGES += \
    wcnss_service \
    libwcnss_qmi

PRODUCT_PACKAGES += \
    p2p_supplicant_overlay.conf \
    wpa_supplicant_overlay.conf

# Pronto symlinks
PRODUCT_PACKAGES += \
    WCNSS_qcom_cfg.ini \
    WCNSS_qcom_wlan_nv.bin \
    wpa_supplicant_overlay.conf \
    p2p_supplicant_overlay.conf \
    wpa_supplicant.conf \
    hostapd_default.conf \
    hostapd.accept \
    hostapd.deny

# Prima firmware
PRODUCT_COPY_FILES += \
    device/lge/madai/wifi/WCNSS_cfg.dat:system/etc/firmware/wlan/prima/WCNSS_cfg.dat \
    device/lge/madai/wifi/WCNSS_qcom_cfg.ini:system/etc/wifi/WCNSS_qcom_cfg.ini \
    device/lge/madai/wifi/WCNSS_qcom_wlan_nv.bin:system/etc/wifi/WCNSS_qcom_wlan_nv.bin

# Tools
PRODUCT_PACKAGES += \
    atrace \
    curl \
    rsync \
    fio \
    lsof \
    strace_static \
    unrar

# Filesystem Tools
PRODUCT_PACKAGES += \
    e2fsck \
    make_ext4fs \
    mke2fs \
    resize2fs \
    setup_fs \
    tune2fs \
    fsck_msdos

# F2FS Tools
PRODUCT_PACKAGES += \
    fsck.f2fs \
    mkfs.f2fs

# EXFAT Tools
PRODUCT_PACKAGES += \
    fsck.exfat \
    mkfs.exfat \
    mount.exfat

# NTFS Tools
PRODUCT_PACKAGES += \
    ntfs-3g \
    ntfsfix \
    mkntfs

# Bash
PRODUCT_PACKAGES += \
    bash \
    bash.bin \
    bashrc \
    inputrc

# Misc
PRODUCT_PACKAGES += \
    curl \
    libbson \
    libcurl \
    libboringssl-compat

# Image Tools
PRODUCT_PACKAGES += \
    simg2img \
    img2simg \
    tcpdump \
    timeinfo

# Log Tools
PRODUCT_PACKAGES += \
    cpio \
    dumpling \
    latencytop \
    procmem \
    procrank \
    taskstats \
    parted

# Filesystem Tools
PRODUCT_PACKAGES += \
    e2fsck \
    e2fsck_static \
    make_ext4fs \
    mke2fs \
    resize2fs \
    setup_fs \
    tune2fs \
    fsck_msdos \
    setup_fs_static \
    make_ext4fs_static

# F2FS Tools
PRODUCT_PACKAGES += \
    fsck.f2fs \
    mkfs.f2fs

# EXFAT Tools
PRODUCT_PACKAGES += \
    fsck.exfat \
    mkfs.exfat \
    mount.exfat

# NTFS Tools
PRODUCT_PACKAGES += \
    ntfs-3g \
    ntfsfix \
    mkntfs

# Crda
PRODUCT_PACKAGES += \
    crda \
    linville.key.pub.pem \
    regdbdump \
    regulatory.bin

# IW & Pals
PRODUCT_PACKAGES += \
	iw \
	iwconfig \
	iwlist \
	iwpriv \
	iwgetid \
	iwspy

PRODUCT_PACKAGES += \
    libstm-log \
    test-stm \
    hwc-test-arrows \
    test-nusensors \
    libcnefeatureconfig \
    libminui

# EGL config
PRODUCT_COPY_FILES += \
    device/lge/madai/rootdir/lib/egl/egl.cfg:system/lib/egl/egl.cfg

# What is this, a radio for ants?
PRODUCT_PACKAGES += \
    antradio_app \
    libantradio

# Nano & Terminfo
PRODUCT_PACKAGES += \
    ansi \
    dumb \
    linux \
    nano \
    pcansi \
    screen \
    unknown \
    vt100 \
    vt102 \
    vt200 \
    vt220 \
    xterm \
    xterm-color \
    xterm-xfree86

PRODUCT_PACKAGES += \
    ksminfo

# Gello Chromium Browser
PRODUCT_PACKAGES += \
    Gello

# Mobicore
PRODUCT_PACKAGES += \
    libMcClient \
    libMcRegistry \
    libPaApi \
    mcDriverDaemon \
    rootpa_interface

# Alljoyn
PRODUCT_PACKAGES += \
    liballjoyn

PRODUCT_PACKAGES += \
    libxml2 \
    libtinyxml2 \
    libstlport \
    EGL_test \
    test-opengl-gralloc \
    libhwcTest \
    angeles \
    libqsap_sdk

# LG other
PRODUCT_PACKAGES += \
	freeblk \
	libshims_wvm \
	libjpeg_turbo \
	libsimd

# LG SDEncryption
PRODUCT_PACKAGES += \
	dumpsexp \
	hmac256 \
	ecryptfs_manager \
	ecryptfs_wrap_passphrase \
	ecryptfs_unwrap_passphrase \
	ecryptfs_insert_wrapped_passphrase_into_keyring \
	ecryptfs_rewrap_passphrase \
	ecryptfs_add_passphrase \
	ecryptfs-stat \
	gcryptrnd \
	getrandom \
	keyctl \
	mkerrcodes \
	mount.ecryptfs \
	libecryptfs \
	libecryptfs_key_mod_passphrase \
	libgcrypt \
	libgpg-error \
	libkeyutils \
	libtimescale_filter \
	request-key


# System property declarations (used for entries with a space or that are empty)
TARGET_SYSTEM_PROP := device/lge/madai/system.prop
# System property overrides
include $(LOCAL_PATH)/system_prop.mk

# OEM Unlock reporting
ADDITIONAL_DEFAULT_PROPERTIES += \
    ro.oem_unlock_supported=1

# In userdebug, add minidebug info the the boot image and the system server to support
# diagnosing native crashes.
ifneq (,$(filter userdebug, $(TARGET_BUILD_VARIANT)))
    # Boot image.
    PRODUCT_DEX_PREOPT_BOOT_FLAGS += --generate-mini-debug-info
    # System server and some of its services.
    # Note: we cannot use PRODUCT_SYSTEM_SERVER_JARS, as it has not been expanded at this point.
    $(call add-product-dex-preopt-module-config,services,--generate-mini-debug-info)
    $(call add-product-dex-preopt-module-config,wifi-service,--generate-mini-debug-info)
endif
