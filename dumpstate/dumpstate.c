/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <dumpstate.h>

void dumpstate_board()
{
    run_command("Block List", 5, "ls", "-lRp", "/dev/block/", NULL);
    run_command("Filesystem List", 5, "ls", "-lRp", "/proc/fs/", NULL);
    dump_file("Mount List", "/proc/mounts");
    dump_file("TWRP Last Install", "/cache/recovery/last_install");
    dump_file("TWRP Log", "/cache/recovery/log");
    dump_file("TWRP Last Log", "/cache/recovery/last_log");
    dump_file("TWRP Last Last Log", "/cache/recovery/last_log.1");

    dump_file("INTERRUPTS", "/proc/interrupts");
    dump_file("Power Management Stats", "/proc/msm_pm_stats");
    run_command("SUBSYSTEM TOMBSTONES", 5, SU_PATH, "root", "ls", "-l", "/data/tombstones/ramdump", NULL);
    dump_file("BAM DMUX Log", "/d/ipc_logging/bam_dmux/log");
    dump_file("SMD Log", "/d/ipc_logging/smd/log");
    dump_file("SMD PKT Log", "/d/ipc_logging/smd_pkt/log");
    dump_file("IPC Router Log", "/d/ipc_logging/ipc_router/log");
    dump_file("dmabuf info", "/d/dma_buf/bufinfo");
    dump_file("Battery Type", "/sys/class/power_supply/bms/battery_type");
    run_command("Temperatures", 5, SU_PATH, "root", "/system/bin/sh", "-c", "for f in emmc_therm msm_therm pa_therm0 xo_therm ; do echo -n \"$f : \" ; cat /sys/class/hwmon/hwmon2/device/$f ; done ; for f in `ls /sys/class/thermal` ; do type=`cat /sys/class/thermal/$f/type` ; temp=`cat /sys/class/thermal/$f/temp` ; echo \"$type: $temp\" ; done", NULL);
    dump_file("dmesg-ramoops-0", "/sys/fs/pstore/dmesg-ramoops-0");
    dump_file("dmesg-ramoops-1", "/sys/fs/pstore/dmesg-ramoops-1");
    run_command("ION HEAPS", 5, SU_PATH, "root", "/system/bin/sh", "-c", "for f in $(ls /d/ion/*); do echo $f; cat $f; done", NULL);
    dump_file("RPM Master Stats", "/d/rpm_master_stats");
    dump_file("RPM stats", "/d/rpm_stats");
    run_command("RPM log", 5, SU_PATH, "root", "/system/bin/sh", "-c", "head -1024 /d/rpm_log", NULL);
};
