#
# Copyright (C) 2017 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

BOARD_VENDOR := lge

TARGET_OTA_ASSERT_DEVICE := madai

TARGET_SPECIFIC_HEADER_PATH += device/lge/madai/include
TARGET_NEEDS_PLATFORM_TEXT_RELOCATIONS=y


# Bootloader
TARGET_NO_BOOTLOADER := true
TARGET_NO_RADIOIMAGE := true
TARGET_BOOTLOADER_BOARD_NAME := madai
BOOTLOADER_PLATFORM := msm8226
BOOTLOADER_GCC_VERSION := arm-eabi-4.8
 
# Platform
TARGET_BOARD_PLATFORM := msm8226
TARGET_BOARD_PLATFORM_GPU := qcom-adreno305

# Architecture
TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_MEMCPY_BASE_OPT_DISABLE := true
TARGET_CPU_VARIANT := krait
# USE_CLANG_PLATFORM_BUILD := true

# Kernel image
TARGET_KERNEL_SOURCE := kernel/lge/madai
TARGET_KERNEL_CONFIG := ffos_madai_defconfig

BOARD_KERNEL_CMDLINE := console=ttyHSL0,115200,n8 androidboot.console=ttyHSL0
BOARD_KERNEL_CMDLINE += user_debug=31 msm_rtb.filter=0x37
BOARD_KERNAL_CMDLINE += androidboot.hardware=madai
BOARD_KERNEL_CMDLINE += androidboot.selinux=permissive

BOARD_KERNEL_SEPARATED_DT := true
BOARD_KERNEL_PAGESIZE	 := 2048
BOARD_KERNEL_BASE		 := 0x00000000
BOARD_RAMDISK_OFFSET 	 := 0x02000000
BOARD_KERNEL_TAGS_OFFSET := 0x01E00000

BOARD_CUSTOM_BOOTIMG := true
BOARD_CUSTOM_BOOTIMG_MK := device/lge/madai/releasetools/mkbootimg.mk

BOARD_MKBOOTIMG_ARGS := --ramdisk_offset $(BOARD_RAMDISK_OFFSET)
BOARD_MKBOOTIMG_ARGS += --tags_offset    $(BOARD_KERNEL_TAGS_OFFSET)


# Storage
BOARD_BOOTIMAGE_PARTITION_SIZE     := 33554432
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 33554432
BOARD_USERDATAIMAGE_PARTITION_SIZE := 13725837312
BOARD_SYSTEMIMAGE_PARTITION_SIZE	 := 1073741824
BOARD_CACHEIMAGE_PARTITION_SIZE		 := 69206016
# BOARD_SYSTEMIMAGE_PARTITION_SIZE   := 2147483648
# BOARD_CACHEIMAGE_PARTITION_SIZE    := 884000000

BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE	 := ext4

# Filesystem
TARGET_USERIMAGES_USE_EXT4 := true
TARGET_USERIMAGES_USE_F2FS := true
BOARD_CANT_BUILD_RECOVERY_FROM_BOOT_PATCH := true 
TARGET_NOT_USE_GZIP_RECOVERY_RAMDISK := true
BOARD_FLASH_BLOCK_SIZE := 131072

BOARD_CANT_BUILD_RECOVERY_FROM_BOOT_PATCH := true 
TARGET_NOT_USE_GZIP_RECOVERY_RAMDISK := true

# Use HW crypto for ODE
TARGET_HW_DISK_ENCRYPTION := true

# QCOM hardware
BOARD_USES_QCOM_HARDWARE := true
PROTOBUF_SUPPORTED := false
TARGET_USES_ION := true
TARGET_USES_NEW_ION_API :=true
TARGET_USES_QCOM_BSP := true

# It is time.
BOARD_USES_QC_TIME_SERVICES := true
TARGET_ENABLE_QC_AV_ENHANCEMENTS := true

# Lights
TARGET_PROVIDES_LIBLIGHT := true

# Audio
BOARD_USES_ALSA_AUDIO := true
AUDIO_FEATURE_ENABLED_HWDEP_CAL := true
AUDIO_FEATURE_ENABLED_MULTI_VOICE_SESSIONS := true
AUDIO_FEATURE_ENABLED_NEW_SAMPLE_RATE := true
USE_CUSTOM_AUDIO_POLICY := 1

# Graphics
MAX_EGL_CACHE_KEY_SIZE := 12*1024
MAX_EGL_CACHE_SIZE := 2048*1024
BOARD_EGL_CFG := device/lge/madai/rootdir/lib/egl/egl.cfg
OVERRIDE_RS_DRIVER := libRSDriver_adreno.so
TARGET_CONTINUOUS_SPLASH_ENABLED := true
TARGET_FORCE_HWC_FOR_VIRTUAL_DISPLAYS := true
TARGET_USES_C2D_COMPOSITION := true
VSYNC_EVENT_PHASE_OFFSET_NS := 2500000
SF_VSYNC_EVENT_PHASE_OFFSET_NS := 0000000

# RIL
TARGET_RIL_VARIANT := caf
FEATURE_QCRIL_UIM_SAP_SERVER_MODE := true
PROTOBUF_SUPPORTED := true

# GPS
BOARD_VENDOR_QCOM_LOC_PDK_FEATURE_SET := true
TARGET_NO_RPC := true

# Enable sensor multi HAL
USE_SENSOR_MULTI_HAL := true

# ANT Wireless
BOARD_ANT_WIRELESS_DEVICE := "qualcomm-smd"

# GPS
BOARD_VENDOR_QCOM_GPS_LOC_API_HARDWARE := default
BOARD_VENDOR_QCOM_LOC_PDK_FEATURE_SET := true
TARGET_NO_RPC := true

# Wifi
BOARD_HAS_QCOM_WLAN := true
BOARD_WLAN_DEVICE := qcwcn
WPA_SUPPLICANT_VERSION := VER_0_8_X
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
BOARD_HOSTAPD_DRIVER := NL80211
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_$(BOARD_WLAN_DEVICE)
BOARD_HOSTAPD_PRIVATE_LIB := lib_driver_cmd_$(BOARD_WLAN_DEVICE)
TARGET_USES_WCNSS_CTRL := true
#TARGET_USES_QCOM_WCNSS_QMI := true
TARGET_PROVIDES_WCNSS_QMI := true
WIFI_DRIVER_FW_PATH_STA := "sta"
WIFI_DRIVER_FW_PATH_AP := "ap"
WLAN_PATH = wlan-caf

# Bluetooth
BOARD_HAVE_BLUETOOTH := true
BOARD_HAVE_BLUETOOTH_QCOM := true
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := device/lge/madai/bluetooth
BLUETOOTH_HCI_USE_MCT := true
QCOM_BT_USE_BTNV := true

# RIL
# BOARD_RIL_CLASS := ../../../device/lge/madai/ril/

# Crypto
TARGET_HW_DISK_ENCRYPTION := true
TARGET_KEYMASTER_WAIT_FOR_QSEE := true

# Camera
USE_CAMERA_STUB := false
# USE_DEVICE_SPECIFIC_CAMERA := true
BOARD_QTI_CAMERA_32BIT_ONLY := true

# NFC
TARGET_USES_NFC := true
BOARD_NFC_CHIPSET := pn547
BOARD_NFC_DEVICE := "/dev/pn547"
BOARD_NFC_HAL_SUFFIX := $(TARGET_BOARD_PLATFORM)

# Power
TARGET_POWERHAL_VARIANT := qcom

# Bootanimation
TARGET_BOOTANIMATION_PRELOAD := true
TARGET_BOOTANIMATION_TEXTURE_CACHE := true

# Media
TARGET_HAVE_SIGNED_VENUS_FW := true

# Hardware
BOARD_USES_CYANOGEN_HARDWARE := true
BOARD_HARDWARE_CLASS := \
    $(COMMON_PATH)/cmhw \
    hardware/cyanogen/cmhw \
	device/lge/madai/cmhw

# Enable features in video HAL that can compile only on this platform
TARGET_USES_MEDIA_EXTENSIONS := true

# Healthd
BOARD_CHARGER_ENABLE_SUSPEND := true
# BOARD_CHARGER_DISABLE_INIT_BLANK := true
BOARD_HEALTHD_CUSTOM_CHARGER_RES := device/lge/madai/charger/images
 
# Offmode Charging
BOARD_CHARGER_ENABLE_SUSPEND := true
BOARD_CHARGER_DISABLE_INIT_BLANK := true
BOARD_CHARGING_CMDLINE_NAME := "androidboot.mode"
BOARD_CHARGING_CMDLINE_VALUE := "chargerlogo"

# Fonts
EXTENDED_FONT_FOOTPRINT := true

# SELinux policies
include device/qcom/sepolicy/sepolicy.mk

BOARD_SEPOLICY_DIRS += device/lge/madai/sepolicy
BOARD_SECCOMP_POLICY:= device/lge/madai/seccomp

## Recovery
TARGET_RECOVERY_UI_LIB := librecovery_ui_msm
TARGET_RECOVERY_UPDATER_LIBS := librecovery_updater_msm
# TARGET_RECOVERY_UPDATER_LIBS := librecovery_updater_madai
TARGET_RELEASETOOLS_EXTENSIONS := device/qcom/common

# Recovery
BOARD_HAS_NO_MISC_PARTITION := true
BOARD_HAS_NO_SELECT_BUTTON := true
BOARD_RECOVERY_SWIPE := true
BOARD_SUPPRESS_EMMC_WIPE := true
BOARD_SUPPRESS_SECURE_ERASE := true
TARGET_RECOVERY_PIXEL_FORMAT := "RGBA_8888"
TARGET_RECOVERY_FSTAB := device/lge/madai/rootdir/fstab.madai

# TWRP Support - Optional
ifeq ($(WITH_TWRP),true)
include device/lge/madai/twrp.mk
endif

# Inherit from QC proprietary
ifneq ($(QCPATH),)
#USESECIMAGETOOL := true
# Gensecimage generation of signed apps bootloader
#QTI_GENSECIMAGE_MSM_IDS := 8x26
# Use signed image as default
#QTI_GENSECIMAGE_SIGNED_DEFAULT := 8x26
#SECIMAGE tool feature flags
#export USES_SEC_POLICY_DEFAULT_SUBFOLDER_SIGN=1
#export USES_SEC_POLICY_INTEGRITY_CHECK=1

-include $(QCPATH)/common/msm8226/BoardConfigVendor.mk
#$(call inherit-product, vendor/qcom/proprietary/prebuilt_HY11/target/product/msm8909/prebuilt.mk)
#$(call inherit-product, vendor/qcom/proprietary/common/config/device-vendor.mk)
endif
