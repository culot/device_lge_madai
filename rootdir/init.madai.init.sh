#!/system/bin/sh
#

# No path is set up at this point so we have to do it here.
PATH=/sbin:/system/sbin:/system/bin:/system/xbin
export PATH

serialno=`getprop ro.serialno`

mount_needed=false;

echo "MADAI(init.madai.init.sh): Begin '$serialno'..." > /dev/kmsg 

if [ ! -f /system/etc/boot_fixup ]; then
# Remount system as read-write.
  echo "MADAI(init.madai.init.sh): Remount needed: '$mount_needed'" > /dev/kmsg 
  mount -o rw,remount,barrier=1 /system
  mount_needed=true;
  echo "MADAI(init.madai.init.sh): Remounting system partition as RW" > /dev/kmsg 
  cp /system/etc/wifi/WCNSS_qcom_wlan_nv.bin /persist/WCNSS_qcom_wlan_nv.bin
fi

    # Set date to a time after 2008
    # This is a workaround for Zygote to preload time related classes properly
date -s 20160711.121212
    echo "MADAI(init.madai.init.sh): Welcome to the 21st century..." > /dev/kmsg 

# Run wifi script
if [ -f /init.madai.wifi.sh ]; then
    /system/bin/sh /init.madai.wifi.sh
    echo "MADAI(init.madai.init.sh): Running init.madai.wifi.sh..." > /dev/kmsg 
fi

touch /system/etc/boot_fixup
chmod 644 /system/etc/boot_fixup

# Remount system as read-only.
if $mount_needed; then
    mount -o ro,remount,barrier=1 /system
    echo "MADAI(init.madai.init.sh): Remounting system partition as RO" > /dev/kmsg 
fi

# Setup HDMI related nodes & permissions
# HDMI can be fb1 or fb2
# Loop through the sysfs nodes and determine
# the HDMI(dtv panel)
for fb_cnt in 0 1 2
do
file=/sys/class/graphics/fb$fb_cnt
dev_file=/dev/graphics/fb$fb_cnt
  if [ -d "$file" ]
  then
    value=`cat $file/msm_fb_type`
    case "$value" in
            "dtv panel")
        chown -h system.graphics $file/hpd
        chown -h system.system $file/hdcp/tp
        chown -h system.graphics $file/vendor_name
        chown -h system.graphics $file/product_description
        chmod -h 0664 $file/hpd
        chmod -h 0664 $file/hdcp/tp
        chmod -h 0664 $file/vendor_name
        chmod -h 0664 $file/product_description
        chmod -h 0664 $file/video_mode
        chmod -h 0664 $file/format_3d
        # create symbolic link
        ln -s $dev_file /dev/graphics/hdmi
        # Change owner and group for media server and surface flinger
        chown -h system.system $file/format_3d;;
    esac
  fi
done

echo "MADAI(init.madai.init.sh): Out." > /dev/kmsg 
