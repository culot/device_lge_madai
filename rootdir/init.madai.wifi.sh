#!/system/bin/sh

# No path is set up at this point so we have to do it here.
PATH=/sbin:/system/sbin:/system/bin:/system/xbin
export PATH

serialno=`getprop ro.serialno`

echo "MADAI(init.madai.wifi.sh): Begin '$serialno'..." > /dev/kmsg 

setprop wlan.driver.ath 0

# Populate the writable driver configuration file
if [ ! -e /data/misc/wifi/WCNSS_qcom_cfg.ini ]; then
    echo "MADAI(init.madai.wifi.sh): Updated WCNSS_qcom_cfg.ini locations." > /dev/kmsg 
    cp /system/etc/wifi/WCNSS_qcom_cfg.ini /data/misc/wifi/WCNSS_qcom_cfg.ini
fi

chown -h system:wifi /data/misc/wifi/WCNSS_qcom_cfg.ini
chmod -h 660 /data/misc/wifi/WCNSS_qcom_cfg.ini

if [ ! -e /system/etc/firmware/wlan/prima/WCNSS_qcom_wlan_nv.bin ]; then
    echo "MADAI(init.madai.wifi.sh): Updated WCNSS_qcom_wlan_nv.bin symlink." > /dev/kmsg
    ln -s /persist/WCNSS_qcom_wlan_nv.bin /system/etc/firmware/wlan/prima/WCNSS_qcom_wlan_nv.bin
fi

if [ ! -e /system/etc/firmware/wlan/prima/WCNSS_qcom_cfg.ini ]; then
    echo "MADAI(init.madai.wifi.sh): Updated WCNSS_qcom_cfg.ini symlink." > /dev/kmsg
    ln -s /data/misc/wifi/WCNSS_qcom_cfg.ini /system/etc/firmware/wlan/prima/WCNSS_qcom_cfg.ini
fi

if [ ! -e /system/etc/firmware/wcd9306/wcd9320_anc.bin ]; then
    echo "MADAI(init.madai.wifi.sh): Updated wcd9306_anc.bin symlink." > /dev/kmsg
    ln -s /data/misc/audio/wcd9320_anc.bin /system/etc/firmware/wcd9306/wcd9320_anc.bin
fi

if [ ! -e /system/etc/firmware/wcd9306/wcd9320_mbhc.bin ]; then
    echo "MADAI(init.madai.wifi.sh): Updated wcd9306_mbhc.bin symlink." > /dev/kmsg
    ln -s /data/misc/audio/mbhc.bin /system/etc/firmware/wcd9306/wcd9320_mbhc.bin
fi

setprop wlan.driver.config /data/misc/wifi/WCNSS_qcom_cfg.ini

# There is a device file.  Write to the file
# so that the driver knows userspace is
# available for firmware download requests
echo 1 > /dev/wcnss_wlan
echo $serialno > /sys/devices/fb000000.qcom,wcnss-wlan/serial_number

echo "MADAI(init.madai.wifi.sh): Inserting wlan.ko!" > /dev/kmsg 
    insmod /system/lib/modules/wlan.ko

echo "MADAI(init.madai.wifi.sh): Out." > /dev/kmsg 

exit 0
