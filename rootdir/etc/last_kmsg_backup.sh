#!/system/bin/sh

integer max_count=30
backup_footprint=/data/footprint
backup_folder=/data/dontpanic
cnt_file=$backup_footprint/reset_count
count_file=$backup_folder/next_count

if /system/bin/ls /proc/last_kmsg ; then
	if /system/bin/ls $count_file ; then
		integer count=`/system/bin/cat $count_file`
		count=$count+0
		case $count in
            "" ) count=0
		esac
	else
		count=0
	fi
	echo [[[[ Written $backup_folder/last_kmsg$count $max_count ]]]]
	/system/bin/cat /proc/last_kmsg > $backup_folder/last_kmsg$count
	/system/bin/cat /proc/cmdline >> $backup_folder/last_kmsg$count
    # reason is att permission certification
    /system/bin/chmod 664 $backup_folder/last_kmsg$count
	count=$count+1
	if  (($count>=$max_count)) ; then
		count=0
		echo restart
	fi
	echo $count > $count_file
    /system/bin/chmod 664 $count_file
fi

if [ -f /sys/module/lge_handle_panic/parameters/crash_footprint ]; then
	footprint=`/system/bin/cat /sys/module/lge_handle_panic/parameters/crash_footprint`
	if [ ! -f $cnt_file ]; then
		reset_cnt=0
		echo $reset_cnt > $cnt_file
		/system/bin/chmod 664 $cnt_file
	fi

	if [ $footprint == "1" ]; then
		if /system/bin/ls /proc/last_kmsg ; then
			/system/bin/cat /proc/last_kmsg > $backup_footprint/last_kmsg
			/system/bin/cat /proc/cmdline >> $backup_footprint/last_kmsg
			/system/bin/chmod 664 $backup_footprint/last_kmsg
			integer cnt=`/system/bin/cat $cnt_file`
			cnt=$cnt+1
			echo $cnt > $cnt_file
			/system/bin/chmod 664 $cnt_file
		fi
	else
		rm -rf $backup_footprint/last_kmsg
	fi
fi
