#!/system/bin/sh
# Copyright (c) 2009-2013, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of The Linux Foundation nor
#       the names of its contributors may be used to endorse or promote
#       products derived from this software without specific prior written
#       permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NON-INFRINGEMENT ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

echo "MADAI(init.madai.sh): Begin..." > /dev/kmsg

# No path is set up at this point so we have to do it here.
PATH=/sbin:/system/sbin:/system/bin:/system/xbin
export PATH

# Set platform variables
if [ -f /sys/devices/soc0/hw_platform ]; then
    soc_hwplatform=`cat /sys/devices/soc0/hw_platform` 2> /dev/null
else
    soc_hwplatform=`cat /sys/devices/system/soc/soc0/hw_platform` 2> /dev/null
fi
if [ -f /sys/devices/soc0/soc_id ]; then
    soc_hwid=`cat /sys/devices/soc0/soc_id` 2> /dev/null
else
    soc_hwid=`cat /sys/devices/system/soc/soc0/id` 2> /dev/null
fi
if [ -f /sys/devices/soc0/platform_version ]; then
    soc_hwver=`cat /sys/devices/soc0/platform_version` 2> /dev/null
else
    soc_hwver=`cat /sys/devices/system/soc/soc0/platform_version` 2> /dev/null
fi

target=`getprop ro.board.platform`

log -t BOOT -p i "MSM target '$target', SoC '$soc_hwplatform', HwID '$soc_hwid', SoC ver '$soc_hwver'"

#
# Function to start sensors for DSPS enabled platforms
#
start_sensors()
{
    if [ -c /dev/msm_dsps -o -c /dev/sensors ]; then
        mkdir -p /data/system/sensors
        touch /data/system/sensors/settings
        chmod -h 775 /data/system/sensors
        chmod -h 664 /data/system/sensors/settings
        chown -h system /data/system/sensors/settings

        chmod -h 775 /persist/sensors
        chmod -h 664 /persist/sensors/sensors_settings
        chown -h system.root /persist/sensors/sensors_settings

        mkdir -p /data/misc/sensors
        chmod -h 775 /data/misc/sensors

        if [ ! -s /data/system/sensors/settings ]; then
            # If the settings file is empty, enable sensors HAL
            # Otherwise leave the file with it's current contents
            echo 1 > /data/system/sensors/settings
        fi
        start sensors
        echo "MADAI(init.madai.sh): Starting sensors service..." > /dev/kmsg 
    fi
}


## GPS
izat_premium_enablement=`getprop ro.qc.sdk.izat.premium_enabled`
izat_service_mask=`getprop ro.qc.sdk.izat.service_mask`

# Suppress default route installation during RA for IPV6; user space will take
# care of this; exception default ifc
for file in /proc/sys/net/ipv6/conf/*
do
  echo 0 > $file/accept_ra_defrtr
done
echo 1 > /proc/sys/net/ipv6/conf/default/accept_ra_defrtr

let "izat_service_gtp_wifi=$izat_service_mask & 2#1"
let "izat_service_gtp_wwan_lite=($izat_service_mask & 2#10)>>1"
let "izat_service_pip=($izat_service_mask & 2#100)>>2"

if [ "$izat_premium_enablement" -ne 1 ]; then
    if [ "$izat_service_gtp_wifi" -ne 0 ]; then
# GTP WIFI bit shall be masked by the premium service flag
        let "izat_service_gtp_wifi=0"
    fi
fi

if [ "$izat_service_gtp_wwan_lite" -ne 0 ] ||
   [ "$izat_service_gtp_wifi" -ne 0 ] ||
   [ "$izat_service_pip" -ne 0 ]; then
# OS Agent would also be started under the same condition
    start location_mq
fi

if [ "$izat_service_gtp_wwan_lite" -ne 0 ] ||
   [ "$izat_service_gtp_wifi" -ne 0 ]; then
# start GTP services shared by WiFi and WWAN Lite
    start xtwifi_inet
    start xtwifi_client
fi

if [ "$izat_service_gtp_wifi" -ne 0 ] ||
   [ "$izat_service_pip" -ne 0 ]; then
# advanced WiFi scan service shared by WiFi and PIP
    start lowi-server
fi

if [ "$izat_service_pip" -ne 0 ]; then
# PIP services
    start quipc_main
    start quipc_igsn
fi

echo "MADAI(init.madai.sh): Start_sensors..." > /dev/kmsg
start_sensors

echo "MADAI(init.madai.sh): End." > /dev/kmsg
